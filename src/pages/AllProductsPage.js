import React from 'react';
import { useSelector } from 'react-redux';

import ProductsList from '../components/products/ProductsList';
import { selectItems } from '../store/products-slice';

const AllProductsPage = props => {
    const productsList = useSelector(selectItems);

    return (
        <ProductsList productsList={productsList} />
    );
};

export default AllProductsPage;