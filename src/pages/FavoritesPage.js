import React from 'react';
import { useSelector } from 'react-redux';

import FavoritesList from '../components/favorites/FavoritesList';
import { selectItems } from '../store/products-slice';

const FavoritesPage = props => {
    const favorites = useSelector(selectItems).filter(item => item.isFavorite);

    return (
        <FavoritesList productsList={favorites} />
    );
};

export default FavoritesPage;