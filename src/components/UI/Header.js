import React from 'react';
import { NavLink } from 'react-router-dom';

import classes from './Header.module.css';

const Header = () => {
    return (
        <div className={classes.header}>
            <nav>
                <span>
                    <NavLink exact to='/' activeClassName={classes.active}>
                        All Products
                    </NavLink>
                </span>
                <span>
                    <NavLink to='/favorites' activeClassName={classes.active}>
                        Favorites
                    </NavLink>
                </span>
            </nav>
        </div>
    );
};

export default Header;