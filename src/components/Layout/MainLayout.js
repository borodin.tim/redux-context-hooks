import Header from '../UI/Header';

import classes from './MainLayout.module.css';

const MainLayout = props => {
    return (
        <div>
            <Header />
            <main className={classes.main}>
                {props.children}
            </main>
        </div>
    );
};

export default MainLayout;