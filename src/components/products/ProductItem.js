import React from 'react';
import { useDispatch } from 'react-redux';

import classes from './ProductItem.module.css';
import Card from '../UI/Card';
import { cartActions } from '../../store/products-slice';

const ProductItem = props => {
    const dispatch = useDispatch();

    return (
        <Card>
            <div className={classes['product-data']}>
                <span>{props.name}</span>
                <span><small>{props.description}</small></span>
            </div>
            <button
                className={props.isFavorite ? classes['button-rev'] : classes.button}
                onClick={() => dispatch(cartActions.toggleFavorite(props.id))}
            >
                {props.isFavorite ? 'Un-favorite' : 'Favorite'}
            </button>
        </Card>
    );
};

export default ProductItem;