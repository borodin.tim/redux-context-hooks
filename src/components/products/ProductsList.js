import React from 'react';

import ProductItem from './ProductItem';

const ProductsList = props => {
    const productsList = props.productsList.map(product => (
        <ProductItem
            key={product.id}
            id={product.id}
            name={product.name}
            description={product.description}
            isFavorite={product.isFavorite}
        />
    ));

    return (
        <div>
            {productsList}
        </div>
    );
};

export default ProductsList;