import React from 'react';

import FavoriteItem from './FavoriteItem';

const FavoritesList = props => {
    const productsList = props.productsList.map(product => (
        <FavoriteItem
            key={product.id}
            name={product.name}
            description={product.description}
            isFavorite={product.isFavorite}
        />
    ));

    return (
        <div>
            {productsList.length === 0 &&
                <p className="centered">
                    No Products Favrited yet
                </p>
            }
            {productsList.length > 0 && productsList}
        </div>
    );
};

export default FavoritesList;