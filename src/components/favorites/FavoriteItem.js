import React from 'react';

import classes from './FavoriteItem.module.css';
import Card from '../UI/Card';

const FavoriteItem = props => {
    return (
        <Card>
            <div className={classes['product-data']}>
                <span>{props.name}</span>
                <span>{props.description}</span>
            </div>
        </Card>
    );
};

export default FavoriteItem;