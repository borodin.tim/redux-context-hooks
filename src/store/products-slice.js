import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    items: [
        {
            id: 'p1',
            name: 'Green T-Shirt',
            description: 'Pretty linen, green shirt',
            isFavorite: false
        },
        {
            id: 'p2',
            name: 'Blue T-Shirt',
            description: 'Nice cotton, blue shirt',
            isFavorite: false
        },
        {
            id: 'p3',
            name: 'Cargo Shorts',
            description: 'Khaki color',
            isFavorite: false
        },
        {
            id: 'p4',
            name: 'Yellow Jacket',
            description: 'Stylish',
            isFavorite: false
        },
        {
            id: 'p5',
            name: 'Merino Wool Hat',
            description: 'Warm and soft',
            isFavorite: false
        },
    ]
};

const productsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        toggleFavorite(state, action) {
            const itemIdx = state.items.findIndex(item => item.id === action.payload);
            state.items[itemIdx].isFavorite = !state.items[itemIdx].isFavorite;
        },
    }
});

export const selectItems = (state) => state.products.items;

export const cartActions = productsSlice.actions;

export default productsSlice.reducer;