import MainLayout from './components/Layout/MainLayout';
import { Switch, Route } from 'react-router-dom';

import AllProductsPage from './pages/AllProductsPage';
import FavoritesPage from './pages/FavoritesPage';

function App() {
  return (
    <MainLayout>
      <Switch>
        <Route path='/' exact>
          <AllProductsPage />
        </Route>
        <Route path='/favorites'>
          <FavoritesPage />
        </Route>
      </Switch>
    </MainLayout>
  );
}

export default App;
